package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x != null && y != null)
            for (int i = 0; i < (x.size() < y.size() ? y.size() : x.size()); i++) {
                if (i < x.size() && x.get(i) == null)
                    throw new IllegalArgumentException();
                if (i < y.size() && y.get(i) == null)
                    throw new IllegalArgumentException();
            } else
            throw new IllegalArgumentException();

        for(int i=0; i<x.size();i++)
            if(!y.contains(x.get(i)))
                return false;

        // в array хранятся индексы элементов,
        // дубликаты которых имеются во входном массиве
        int [] array = new int [x.size()];

        for(int i=0;i<x.size();i++) {
            for (int j = 0; j < y.size(); j++)
                if (y.get(j).equals(x.get(i))) {
                    array[i] = j;
                    break;
                }
        }
        for(int i=1;i<x.size();i++)
            if(array[i-1]>array[i])
                return false;
        return true;
    }
}
