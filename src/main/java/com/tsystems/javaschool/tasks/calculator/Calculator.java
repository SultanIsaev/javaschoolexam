package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Stack;
import java.util.StringTokenizer;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement == null || statement.isEmpty())
            return null;

        double numA;
        double numB;
        String sTmp;
        StringTokenizer st;
        Stack<Double> stack = new Stack<>();

        statement = invertToPostfixNotation(statement);
        if(statement == null)
            return null;
        else
            st = new StringTokenizer(statement);

        while (st.hasMoreTokens()) {
            sTmp = st.nextToken().trim();
            if (1 == sTmp.length() && isOperator(sTmp.charAt(0))) {
                if (stack.size() < 2) {
                    //throw new RuntimeException("Неверное количество данных в стеке для операции " + sTmp);
                    return null;
                }
                numB = stack.pop();
                numA = stack.pop();
                switch (sTmp.charAt(0)) {
                    case '+':
                        numA += numB;
                        break;
                    case '-':
                        numA -= numB;
                        break;
                    case '/':
                        numA /= numB;
                        break;
                    case '*':
                        numA *= numB;
                        break;
                    default:
                        //throw new RuntimeException("Недопустимая операция " + sTmp);
                        return null;
                }
                stack.push(numA);
            } else {
                try {
                    numA = Double.parseDouble(sTmp);
                    stack.push(numA);
                }catch (NumberFormatException e){
                    //e.printStackTrace();
                    return null;
                }
            }
        }

        if (stack.size() > 1) {
            //throw new RuntimeException("Количество операторов не соответствует количеству операндов");
            return null;
        }

        try {
            if(stack.peek()%1 == 0)
                return String.valueOf(new BigDecimal(stack.pop()).setScale(0));
            else
                return String.valueOf(new BigDecimal(stack.pop()).setScale(4, RoundingMode.HALF_UP).stripTrailingZeros());
        }catch (NumberFormatException e){
            //e.printStackTrace();
            return null;
        }
    }

    /**
     * Convert the string to polish (postfix) notation
     * @param sIn Input string
     * @return Output string in reverse postfix notation
     */
    private static String invertToPostfixNotation(String sIn){
        StringBuilder sbStack = new StringBuilder("");  //строка операторов
        StringBuilder sbOut = new StringBuilder("");    //выходная строка
        char cIn;
        char cTmp;

        for (int i = 0; i < sIn.length(); i++) {
            cIn = sIn.charAt(i);
            if (isOperator(cIn)) {
                while (sbStack.length() > 0) {
                    cTmp = sbStack.substring(sbStack.length()-1).charAt(0);
                    if (isOperator(cTmp) && (opPriority(cIn) <= opPriority(cTmp))) {
                        sbOut.append(" ").append(cTmp).append(" ");
                        sbStack.setLength(sbStack.length()-1);
                    } else {
                        sbOut.append(" ");
                        break;
                    }
                }
                sbOut.append(" ");
                sbStack.append(cIn);
            } else if ('(' == cIn) {
                sbStack.append(cIn);
            } else if (')' == cIn) {
                cTmp = sbStack.substring(sbStack.length()-1).charAt(0);
                while ('(' != cTmp) {
                    if (sbStack.length() < 1) {
                        //throw new Exception("Ошибка разбора скобок. Проверьте правильность выражения.");
                        return null;
                    }
                    sbOut.append(" ").append(cTmp);
                    sbStack.setLength(sbStack.length()-1);
                    if(sbStack.length()>0)
                        cTmp = sbStack.substring(sbStack.length()-1).charAt(0);
                    else
                        return null;
                }
                sbStack.setLength(sbStack.length()-1);
            } else {
                // Если символ не оператор - добавляем в выходную последовательность
                sbOut.append(cIn);
            }
        }

        // Если в стеке остались операторы, добавляем их в входную строку
        while (sbStack.length() > 0) {
            sbOut.append(" ").append(sbStack.substring(sbStack.length()-1));
            sbStack.setLength(sbStack.length()-1);
        }

        return  sbOut.toString();
    }

    /**
     * The function checks whether the current character is an operator
     * @param c Input char
     * @return boolean value is operator or not
     */
    private static boolean isOperator(char c) {
        switch (c) {
            case '-':
            case '+':
            case '*':
            case '/':
                return true;
        }
        return false;
    }

    /**
     * Returns the priority of operation
     * @param op char
     * @return byte
     */
    private static byte opPriority(char op) {
        switch (op) {
            case '*':
            case '/':
                return 2;
        }
        return 1; // если + или -
    }
}
