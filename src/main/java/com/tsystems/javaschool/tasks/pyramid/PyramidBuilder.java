package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] pyramid;
        int height = 0; // количество строк массива
        int width;      // количество столбцов

        if (inputNumbers.contains(null) || inputNumbers.size() > 100)
            throw new CannotBuildPyramidException("Invalid input list");

        List<Integer> inNumbers = new ArrayList<>(inputNumbers);
        Collections.sort(inNumbers);

        int size = inNumbers.size();
        // вычисляем height - количество строк массива
        for (int i = 0; i < inNumbers.size(); i++) {
            size-=i;
            if (size > 0)
                height++;
        }

        int current = 0;
        width = height * 2 - 1; // вычисляем количество столбцов массива
        pyramid = new int[height][width];
        for (int i = 0; i < height; i++) {
            // по индексу k мы вставляем
            // значения через каждые 2 ячейки,
            // чтобы 'построить' пирамидальный вид
            int k = height - i - 1;
            for (int j = 0; j <= i; j++) {
                pyramid[i][k] = inNumbers.get(current);
                current++;
                k+=2;
            }
        }
        return pyramid;
    }
}